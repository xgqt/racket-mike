# Mike

<p align="center">
    <a href="http://pkgs.racket-lang.org/package/mike">
        <img src="https://img.shields.io/badge/raco_pkg_install-mike-aa00ff.svg">
    </a>
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-mike">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-mike/">
    </a>
    <a href="https://gitlab.com/xgqt/racket-mike/pipelines">
        <img src="https://gitlab.com/xgqt/racket-mike/badges/master/pipeline.svg">
    </a>
</p>

Micro Make replacement.


## About

This package is a small tool to replace the way I had used
Makefiles in my Racket projects.


### Installation

```sh
raco pkg install --name mike
```


## License

SPDX-License-Identifier: GPL-2.0-or-later

Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
